# rgb-lamps-leds-presentation

I'm playing with RGB bulbs and LED strips.

I want to control them programmatically, directly.  Partly because "cloud" control is problematic, but mostly just because it's fun!

[see as presentation](https://cicero.xyz/v3/remark/0.14.0/gitlab.com/cben/rgb-lamps-leds-presentation/master/README.md)

---

## Part 1: Yeelight RGB lamps (wifi)

Xiaomi's "Yeelight" series has a lot of lighting devices.

Cheap, no longer cheapest.  The standard E27 bulbs (be careful to get 220V-capable ones, they also have US 110V-only ones) are ~22$.

_Mostly_ controllable by same app(s).

Proprietary discovery & initial pairing with app, but then can enable "LAN control" that's simple JSON over TCP, quite well documented.  Roughly same API for all devices.
https://www.yeelight.com/en_US/developer

---

### App drawbacks

- 2 apps — "Yeelight" vs "Mi Home", not all actions possible in both
- app only lets me pick from subset of full RGB color space(?)
- "timer" limited to 60min
- "Automations" / "Schedules" work via Xiaomi's cloud.
- Mi Home "Automations" does work, but awkward
- time settings awkward (time AND a period?! I use period "all day")
- some actions — like basic "turn on and set light" unclear how to parametrize — allow preset "scenes" but not arbitrary RGB?!
- IFTTT integration — exists, bad usability
- google/alexa integrations — exist didn't try

---

### Live demo: JSON over netcat

`nc -C` (`--crlf`) is important

"music mode" necessary to lift command rate limit

---

### Live demo: https://github.com/cben/animation-stack-language

`yeelight` branch

---

## Part 2: apa102 LED strips

Each LED individually addressable!  Essentially a huge shift register.

2 wires: Time + Data, most convenient for driving — as slow as you want, but also fast

5V, possible to power directly from Raspberry Pi (sloppy, works for me, don't chain them too much)

https://pimylifeup.com/raspberry-pi-led-strip-apa102/
